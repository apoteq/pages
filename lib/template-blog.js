import { html, includeLess, less } from './prelude.js'
import { Template } from './template.js'

import * as marked from 'marked'
import matter from 'gray-matter'

export function renderBlogPost(content) {
	const page = new Template()

	page.style += less`
	@desktop: ~"(min-width: 960px)";
	@tablet: ~"(max-width: 1024px)";

	  .content {
	    padding: 0vh 40vh;
	    @media @tablet {
	    	padding: 2vh 2vh;
	    }

	  }
	  .buttons {
	    display: flex;
	    justify-content: space-between;
	    font-size: var(--menuFontSize);
	  }
	  .blog {
	    .title {
	      font-size: 90px;
				@media @tablet {
  			  font-size: 50px;
				}
	    }
	    .title, .subtitle {
	      margin: 0 auto;
	      text-align: center;
	    }
			.text {
				text-align: justify;
				text-align-last: left;
			}
	  }
	`

	const frontMatter = matter(content)
	
	if (!frontMatter.data.titel) {
		throw new Error(`Kein titel gesetzt. Bitte setze den front matter header in der .md datei wie folgt:
---
title: Hier dein titel
untertitel: Hier dein Untertitel
datum: 22.12.2023
---
Dies ist __markdown__`)


	}
	const titel = frontMatter.data.titel
	const untertitel = frontMatter.data.untertitel
	const text = marked.parse(frontMatter.content)
	const mastodon = frontMatter.data.mastodon


	page.content = html`
	.blog {
	  .buttons.noprint {
	    .back {
	      a(href="/#texte"): "<< Zurück"
	    }
	    .print {
	      a(onclick="window.print();" href=""): "Drucken"
	   
	    }
	  
	  }
	  .title: ${titel}
	  .subtitle: ${untertitel}
	  hr 
	  br {}
	  .text: ${text}
	  a.noprint(href=${mastodon}): "Kommentare"
	  br {}
	  br {}

	}
`
	return page
}
