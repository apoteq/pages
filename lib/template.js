import { html, includeLess } from './prelude.js'
import { toAST } from 'nox-html/ast' 
import { Tokenizer } from 'nox-html/tokenizer' 

export class Template {
  title = ""

  script = ''
  style = includeLess('./template.less')

  head = () => html`
    title: "APOTEQ"
    style: ${this.style}
    meta(charset='utf-8')
    meta(name='viewport', content='width=device-width,initial-scale=1')

    meta(
      name='keywords',
      content=''
    )
    link(rel="icon" href="assets/favicon.svg")
    ${false && html`script(src='http://localhost:8123/nox-notify.js')` || ''}
  `

  header = () => ""

  content = ""

  skeleton = () => {
    return html`
    !DOCTYPE(html)
    html(xmlns="http://www.w3.org/1999/xhtml" lang="de" "xml:lang"="de") {
      head: ${this.head}
      body {
        ${this.header}
        .content: ${this.content}
        script: ${this.script}
      }
    }
    `
  }

  toString() {
    return html`${this.skeleton}`
  }
}
