This repository contains the homepage for apoteq.org.

# About

The way this webpage is built is using a set of tools and scripts. This shall simplifiy the work of creating static HTML/CSS webpages.

# Setup

1. To start editing the webpage you should install `nodejs` and `npm`
  - `sudo apt install nodejs npm`
2. Execute `npm install` to setup this project
3. Run `npm start` to start a webserver hosting this repository on `http://127.0.0.1:8080` for you
  - Open `http://127.0.0.1:8080` in a browser
4. Now you can edit & save eg. the `index.html.js` file
5. Refresh your browser and see the changes :)

# Publishing
To publish your changes to `https://apoteq.org` commit and push your changes to the `main` branch of this repository. If you want someone else to have a look on your changes, push to another branch and open a Pull Request.

Please make sure that you add & commit all your files. The source files `*.html.js` and the generated files `*.html`

# Structure

```
|-- index.html        Generated .html file from index.html.js
|-- index.html.js     Source code for the index.html file
|-- lib/              Folder containing the template code, util methods etc.`
|   |-- template.js   This is the template or the general skeleton for the webpage. It can get reused accross pages
|   |-- template.less This is the CSS code for the template, written in less, a language that extends CSS
|   |-- utils.js      This file contains handy utility methods
|   |-- prelude.js    This file is a shortcut to easily load all needed methods/templates into a page
|-- assets/           This folder contains all images/assets for the webpage
|-- bin/              The bin folder contains all the scripts
|-- design/           For sketches, screen designs, mockups, ideas etc
```

