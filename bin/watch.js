#!/usr/bin/env node

import chokidar from 'chokidar'
import { exec } from 'child_process'
import { ___dirname, setLogLevel, logInfo, logDebug, registerSSGErrorHandler, includeFile } from 'nox-utils'
import { join } from 'path'

registerSSGErrorHandler()
setLogLevel(1)


const __dirname = ___dirname(import.meta)

function build(cb) {
  exec(`node ${join(__dirname, './build.js')}`, {maxBuffer: 20000 * 1024}, (error, stdout, stderr) => {
    if (error) {
      console.log(`Error: ${error}`)
      return cb(stdout)
    }
    if (stderr) {
      console.log(`Stderr: ${stderr}`)
      return cb(stdout)
    }
    console.log(`${stdout}`)
    cb()
  })
}

async function startNotifierServer() {
  const http = await import('node:http');

  let open_connections = []

  let status = { current: { type: 'status', status: 'ok', data: '' }}

  const server = http.createServer((req, res) => {
    //@ts-ignore
    const request_path = new URL(req.url, `http://${req.headers.host}`)
    logDebug(`[NotifierServer] request pathname: ${request_path.pathname}`)

    // Set CORS headers
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Request-Method', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
    res.setHeader('Access-Control-Allow-Headers', '*');
    if (request_path.pathname === '/api/pollEvent') {
      res.writeHead(200, {'Content-Type': 'application/json; charset=utf8'});
      open_connections.push(res)
    } else if (request_path.pathname === '/api/status') {
      res.writeHead(200, {'Content-Type': 'application/json; charset=utf8'});
      res.end(JSON.stringify(status.current))
    } else if (request_path.pathname === '/nox-notify.js') {
      res.writeHead(200, {'Content-Type': 'application/javascript'});
      res.end(includeFile('./nox-notify.browser.js', 2))
    } else {
      res.end('HTTP/1.1 404 Not Found\r\n\r\n')
    }
  })
  server.on('clientError', (err, socket) => {
    logDebug(`[NotifierServer] clientError: ${err}`)
    socket.end('HTTP/1.1 400 Bad Request\r\n\r\n')
  })

  const port = 8123
  server.listen(port)
  logInfo(`[NotifierServer] Listening on port ${port}`)

  function sendEventToOpenConnections(event) {
    while (open_connections.length > 0) {
      const res = open_connections.pop()
      res.end(JSON.stringify(event))
      logDebug(`[NotifierServer] closed connection`)
    }
  }

  return function onEvent(event) {
    logDebug(`[NotifierServer] #onEvent() event: ${JSON.stringify(event)}`)

    if (event.type = 'status') {
      status.current = { type: 'status', status: event.status, data: event.data }
    } else if (event.type = 'filesystem_change') {
    }
    sendEventToOpenConnections(event)
  }
}

export function buildAndNotify(notifier_server_send_event) {
  build(err => {
    if (err) return notifier_server_send_event({type: 'status', status: 'fails', data: err})
    notifier_server_send_event({type: 'filesystem_change', data: null})
    notifier_server_send_event({type: 'status', status: 'ok', data: null})
  })
}

async function main() {
  const notifier_server_send_event = await startNotifierServer() 
  buildAndNotify(notifier_server_send_event)

  chokidar
    .watch([
      join(__dirname, '../lib'),
      join(__dirname, './*.browser.js'),
      join(__dirname, '../*.js'),
      join(__dirname, '../*.less'),
      join(__dirname, '../texte/*')
    ])
    .on('all', (event, path) => {
      if (event === 'add' || event === 'addDir') {
        logInfo(`Watching ${path}`)
        return
      }

      logInfo(`${path} changed`)
      buildAndNotify(notifier_server_send_event)
    })
}

main()
