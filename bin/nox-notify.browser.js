function docReady(fn) {
  // see if DOM is already available
  if (document.readyState === "complete" || document.readyState === "interactive") {
      // call on next available tick
      setTimeout(fn, 1)
  } else {
      document.addEventListener("DOMContentLoaded", fn)
  }
}    

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms))
}


docReady(async () => {
  let was_offline = false
  while (true) {
    try {
      const status = await fetch('http://localhost:8123/api/status')
      console.log('[NotifierServer] Connected')

      if (was_offline) {
        window.location.reload()
        return
      }

      const status_json = await status.json()
      console.debug(status_json)
      if (status_json.status === 'fails') {

        document.body.innerHTML = '<pre>' + status_json.data + '</pre>'
      }



      while (true) {
        const response = await Promise.race([
          fetch('http://localhost:8123/api/pollEvent'),
        ])
        const response_json = await response.json()
        console.log('on watch event', response_json)

        if (response_json.type === 'filesystem_change' || response_json.type === 'status') {
          window.location.reload()
        }
      }
    } catch (err) {
      console.log(`[err] ${err}`)
      await sleep(1000)
      was_offline = true
    }
  }
})
