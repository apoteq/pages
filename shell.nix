{ pkgs ? import <nixpkgs> { } }:
pkgs.mkShell rec {
  # Additional tooling
  buildInputs = with pkgs; [
    nodejs_20
    typescript-language-server
  ];  
}
