import { html, includeLess } from './lib/prelude.js'
import { Template } from './lib/template.js'


export const page = new Template()

page.style += includeLess('./index.nox.less')

const Date = ({past, title, date}) => html`
  .date {
    .time(class=${past ? 'past' : ''}): ${date}
    .title: ${title}
  }
`


const Menu = ({children}) => html`
  .menu.noprint {
    .items: ${children}
    .nobordersnonations
  }
`

const MenuItem = ({href, name}) => html`
  .item: a(href=${href}) : ${name}
`

page.content = html`
  ${Menu} {
    ${MenuItem}(name="Kalender" href="#kalender")
    ${MenuItem}(name="Slides" href="#slides")
    ${MenuItem}(name="Texte" href="#texte")
    ${MenuItem}(name="Kontakt" href="#kontakt")
  }
  .pages {
    .page#intro: .container {
      .logo.center: img(src="./assets/apoteq-logo.svg")
      .title.center: "APOTEQ"
      .sub.center: "anarchist queer feminist tech glitch."
      .button-scroll.center:a(href="#kalender"):img.down(src="./assets/button-down.svg")
    }
    .page#kalender: .container {
      .box.dates {
        .title: "Kalender"
        ${Date}(date="13.11.2024 11:00" title="Digitale Selbstverteidigung" past)
        ${Date}(date="04.07.2024 19:00" title="Digitale Selbstverteidigung" past)
        ${Date}(date="02.02.2023 19:00" title="Digitale Selbstverteidigung" past)
        ${Date}(date="24.11.2022 19:00" title="Emailverschlüsselung" past)
        ${Date}(date="13.10.2022 19:00" title="Festplattenverschlüsselung Hands-On" past)
        ${Date}(date="29.09.2022 19:00" title="Einführung in die Festplattenverschlüsselung" past)
      }
    }
    .page#slides: .container {
      .box.slides {
        .title: "Slides"
        .slide {
          .name: "Digitale Selbstverteidigung"
          .download:a(href="https://codeberg.org/apoteq/workshops/raw/branch/main/Digitale Selbstverteidigung/slides.odp", download): "Download"
        }
        .slide {
          .name: "Festplattenverschlüsselung"
          .download:a(href="https://codeberg.org/apoteq/workshops/raw/branch/main/Festplattenverschluesselung/slides-festplattenverschluesselung.odp", download): "Download"
        }
        .slide {
          .name: "Emailverschlüsselung"
          .download:a(href="https://codeberg.org/apoteq/workshops/raw/branch/main/Emailverschluesselung/slides-emailverschluesselung.odp", download): "Download"
        }
      }
    }
    .page#texte : .container {
      .box.texte#texte {
        .title: "Texte"
        .text {
          .titleAndDate {
            .title: "Veracrypt Guide"
            .date: "04.07.2024"
          }
          .link:a(href="https://rrzk.uni-koeln.de/en/informationssicherheit/it-sicherheit/verschluesselung-von-daten/verschluessellung-mit-veracrypt"): "Link"
        }

        .text {
          .titleAndDate {
            .title: "Nerdsplaining - Feministische Kritik an nerdscher Redekultur"
            .date: "21.02.2023"
          }
          .link:a(href="nerdsplaining.html"): "Link"
        }
      }
    }
    .page#kontakt:.container {
      .box.contact {
        .title: "Kontakt"
        .keyValue.mail {
          .key: "E-Mail"
          .value: a(href="mailto:apoteq@riseup.net", alt="email adress"):"apoteq@riseup.net"
        }
        .keyValue.pgp: {
          .key: "PGP"
          .value.mono: {
            a(download="apoteq.asc" href="apoteq.asc", alt="pgp public key file and hash"): "C275 F856 DDDD 3FB6 BC1B
  C535 0BF4 44D9 4666 BDA0"
          }
        }
        .keyValue.mastodon {
          .key: "Mastodon"
          .value:a(href="https://nerdculture.de/@apoteq", alt="mastodon url"):"@apoteq@nerdculture.de"
        }
      }
    }
  }
`
